import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ListEquipement from './ListEquipement';
import Equipement from './Equipement';
import { Route, BrowserRouter } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
	<BrowserRouter>
		<div className='app'>
			<Route exact path='/' component={ListEquipement} />
			<Route path='/equipement/:id' component={Equipement} />
		</div>
	</BrowserRouter>
	, document.getElementById('root'));
registerServiceWorker();
