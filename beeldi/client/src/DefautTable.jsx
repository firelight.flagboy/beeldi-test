import React, { Component } from 'react';

class DefautTable extends Component {
	constructor({ lstDefaut }) {
		super()
		this.state = {
			lstDefaut: lstDefaut
		}
	}

	getHead = () => {
		// get head
		const key = ["#", "type de controle", "point de controle", "commentaire", "defaut",
			"preconisation", "photo"]
		return (
			<thead className='title'>
				<tr>
					{key.map((value, index) => (
						<th scope='col' key={index}>{value}</th>
					))}
				</tr>
			</thead>
		)
	}

	getCellDefaut = (value, key, index) => {
		// get cell content
		let tabTh = [];
		for (let i = 0; i < key.length; i++) {
			tabTh.push(
				<td key={`${index}-${i + 1}`}>{value[key[i]]}</td>
			)
		}
		return (tabTh)
	}

	getImgDefaut = (nomPhoto) => {
		// add img if name exist
		if (nomPhoto.length <= 0)
			return (null);
		return (
			<img
				className='icone defaut'
				style={{height: '100px'}}
				src={`http://localhost:8080/api/photos/${nomPhoto}`}
				alt={nomPhoto} />
		)
	}
	getBody = (lstDefaut) => {
		const key = ["typeControle", "pointDeControle", "commentaire", "defautTexte",
			"preconisation"]
		return (
			<tbody>
				{lstDefaut.map((value, index) => (
					// add a row
					<tr key={index}>
						<th scope='row' key={`${index}-${0}`}>{index}</th>
						{this.getCellDefaut(value, key, index)}
						<td key={`${index}-${6}`}>
							{this.getImgDefaut(value.nomPhoto)}
						</td>
					</tr>
				))}
			</tbody>
		)
	}

	render() {
		const { lstDefaut } = this.state;
		return (
			<div className='defaut rounded m-3 p-3 bg-light'>
				<table className='table table-hover'>
					{this.getHead()}
					{this.getBody(lstDefaut)}
				</table>
			</div>
		)
	}
}

export default DefautTable
