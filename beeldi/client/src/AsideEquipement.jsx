import React, { Component } from 'react';

class AsideEquipement extends Component {

	constructor({ data }) {
		super();
		this.state = {
			data: data,
		}
	}

	getRow = (key, name, equip) => (
		key.map((value, index) => {
			let content = equip[value];
			if (content.length <= 0) {
				content = 'N/A';
			}
			return (
				<tr
					key={index}>
					<th scope='row'>{name[index]}</th>
					<td>{content}</td>
				</tr>
			)
		})
	)
	addInfo = (equip) => {
		// key utiliser pour recupere les info
		const key = ["info_anneeMiseEnService", "info_batiment", "info_domaine",
			"info_niveau", "info_local", "info_marque", "info_modele"]
		const name = ['Mise en service', 'Batiment', 'Domaine', 'Niveaux', 'Local',
			'Marque', 'Modele'];
		return (
			<table className='table table-hover'>
				<caption>Info table</caption>
				<tbody>
					{this.getRow(key, name, equip)}
				</tbody>
			</table>
		)
	}

	addTech = (equip) => {
		const key = ["tech_quantite", "tech_serialNumber", "tech_statut", "tech_priseDeNotes"]
		const name = ["quantite", "serial Number", "statut", "Notes"]

		return (
			<table className='table table-hover'>
				<caption>Tech table</caption>
				<tbody>
					{this.getRow(key, name, equip)}
				</tbody>
			</table>
		)
	}

	addImg(nomPhoto) {
		if (nomPhoto.length <= 0)
			return (null);
		return (
			<img className='w-100'
				src={`http://localhost:8080/api/photos/${nomPhoto}`}
				alt={nomPhoto} />
		)
	}

	render() {
		const { data } = this.state;
		return (
			<aside className='m-3 p-3 rounded w-30 bg-light'>
				<header>
					{this.addImg(data.nomPhotoEquipement)}
				</header>
				<section className='py-3'>
					<h3>{data.nom}</h3>
				</section>
				<footer>
					{this.addInfo(data)}
					{this.addTech(data)}
				</footer>
			</aside>
		)
	}
}

export default AsideEquipement
