import React, { Component } from 'react';
import './MoveUp.css';

class MoveUp extends Component {

	handle = () => {
		window.scrollTo(0, 0);
	}
	render() {
		return (
			<div
				className='moveup'
				onClick={this.handle}>
					<i className="fas fa-angle-double-up"></i>
					Move Up
			</div>
		)
	}
}

export default MoveUp
