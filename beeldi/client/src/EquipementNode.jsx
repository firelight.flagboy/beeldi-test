import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ClipLoader } from 'react-spinners';
import axios from 'axios';
import './EquipementNode.css';

class EquipementNode extends Component {
	constructor({ data }) {
		super()
		this.state = {
			isLoad: false,
			data: data,
			defaut: 0
		}
	}

	componentDidMount() {
		axios.get('http://localhost:8080/api/equipement/query/nb/' + this.state.data.__equipementID)
			.then((reply) => {
				this.setState({ defaut: reply.data, isLoad: true })
			})
	}

	getImg(nomPhotoEquipement) {
		if (nomPhotoEquipement.length <= 0)
			return (<div></div>);
		return (
			<img className="EquipementNode icone" src={`http://localhost:8080/api/photos/${nomPhotoEquipement}`} alt={nomPhotoEquipement} />
		)
	}

	loading() {
		return (
			<div>
				<ClipLoader
					color={'#0000ff'}
					loading={true}
				/>
			</div>
		)
	}

	addInfo(data, isLoad, defaut) {
		if (!isLoad)
			return this.loading()
		return (
			<table className='table'>
				<tbody>
					<tr>
						<th scope='row'>Domaine</th>
						<td>{data.info_domaine}</td>
					</tr>
					<tr>
						<th scope='row'>Defaut</th>
						<td>{defaut}</td>
					</tr>
				</tbody>
			</table>
		)
	}
	render() {
		const { data, isLoad, defaut } = this.state;
		return (
			<Link className="text-truncate badge badge-light d-flex rounded m-3 p-3 row" to={`/equipement/${data.__equipementID}`}>
				<div className='col-lg-2'>
					{this.getImg(data.nomPhotoEquipement)}
				</div>
				<div className="content col-6">
					<h3>{data.nom}</h3>
					{this.addInfo(data, isLoad, defaut)}
				</div>
			</Link>
		)
	}
}

export default EquipementNode
