import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { ClipLoader } from 'react-spinners';
import DefautTable from './DefautTable.jsx';
import AsideEquipement from './AsideEquipement.jsx';
import MoveUp from './MoveUp.jsx';
import './Equipement.css';

class Equipement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: {},
			lstDefaut: [],
			isLoad: false,
			error: false,
			id: this.props.match.params.id
		}
	}

	componentDidMount() {
		axios.get('http://localhost:8080/api/defaut/query/id/' + this.state.id)
			.then((lstDefaut) => {
				axios.get('http://localhost:8080/api/equipement/query/id/' + this.state.id)
					.then((data) => {
						this.setState({
							data: data.data[0],
							lstDefaut: lstDefaut.data,
							isLoad: true,
							error: false
						})
						document.title = data.data[0].nom;
					})
					.get((err) => {
						this.setState({ error: true, isLoad: true })
					})
			})
			.catch((err) => {
				this.setState({ error: true, isLoad: true })
			})
	}

	getContent(lstDefaut, data) {
		return (
			<div className='equip'>
				<DefautTable
					lstDefaut={lstDefaut}
				/>
				<AsideEquipement
					data={data}
				/>
			</div>
		)
	}

	loading() {
		return (
			<div
				className="App equip"
			>
				<div className='loading w-100'>
					<ClipLoader
						color={'#0000ff'}
						loading={true}
					/>
				</div>
			</div>
		)
	}

	error() {
		return (
			<div>
				<div className='comeBack'>
					<Link
						to='/'>
							<i className="fas fa-angle-double-left"></i>
							Retour
					</Link>
				</div>
				<p className='text-center'>An error ocur</p>
				<MoveUp />
			</div>
		)
	}

	render() {
		const { isLoad, error, data, lstDefaut } = this.state;
		if (!isLoad)
			return this.loading();
		if (error)
			return this.error();
		return (
			<div className="App">
				<div className='comeBack'>
					<Link
						to='/'>
							<i className="fas fa-angle-double-left"></i>
							Retour
					</Link>
				</div>
				{this.getContent(lstDefaut, data)}
				<MoveUp />
			</div>
		)
	}
}

export default Equipement
