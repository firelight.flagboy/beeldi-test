import React, { Component } from 'react';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
import EquipementList from './EquipementList.jsx';
import DomainInput from './DomainInput.jsx';
import MoveUp from './MoveUp.jsx'
import './ListEquipement.css';

class ListEquipement extends Component {
	constructor() {
		super();

		this.state = {
			list: [],
			name: '',
			isLoaded: false,
			error: false
		}
	}

	componentDidMount() {
		axios.get('http://localhost:8080/api/equipement/list')
			.then((res) => {
				this.setState({ list: res.data, isLoaded: true, error: false }, () => {
					if (this.state.name.length > 0)
						document.title = this.state.name
					else
						document.title = 'liste des equipements'
				});
			})
			.catch((err) => {
				this.setState({ error: true, isLoaded: true });
			})
	}

	handleRequest = ({ name, type }) => {
		this.setState({ isLoaded: false, list: [] });
		axios.get(`http://localhost:8080/api/equipement/query/${type}/${name}`)
			.then((res) => {
				this.setState({ list: res.data, name: name, isLoaded: true, error: false }, () => {
					if (this.state.name.length > 0)
						document.title = this.state.name
					else
						document.title = 'liste des equipements'
				});
			})
			.catch((err) => {
				this.setState({ error: true, isLoaded: true });
			})
	}

	loading() {
		return (
			<div className='App List'>
				<div className='loading'>
					<ClipLoader
						color={'#0000ff'}
						loading={true}
					/>
				</div>
			</div>
		)
	}

	error() {
		return (
			<div className="App List">
				<DomainInput onSubmit={this.handleRequest} />
				<p className='text-center'>an Error ocur</p>
				<MoveUp />
			</div>
		)
	}

	render() {
		const { isLoaded, list, error } = this.state;
		if (!isLoaded)
			return this.loading()
		if (error)
			return this.error()
		return (
			<div
				className="App List"
			>
				<DomainInput onSubmit={this.handleRequest} />
				<EquipementList lstEquipement={list} />
				<MoveUp />
			</div>
		)
	}
}

export default ListEquipement
