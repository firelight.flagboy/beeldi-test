import React, { Component } from 'react';

class DomainInput extends Component {
	constructor({ onSubmit }) {
		super();
		this.state = {
			name: '',
			type: 'domaine'
		}
		this.onSubmit = onSubmit
	}

	handleInput = (event) => {
		this.setState({ name: event.target.value.toUpperCase() }) // force string to be in upperCase
	}

	handleSelect = (event) => {
		this.setState({ type: event.target.value.toLowerCase() });
		console.log(event.target.value);
	}

	handleSubmit = (event) => {
		const { name, type } = this.state;
		event.preventDefault(); // when the user click on the submit botton prevent reload of the page
		this.onSubmit({ name, type }); // send info to the parent
	}

	render() {
		return (
			<form
				className='DomainInput text-center'
				onSubmit={this.handleSubmit}>
				<div className='form-group d-flex center'>
					<select
						name="request"
						id="request-type"
						className='form-control m-3 col-md-4'
						onChange={this.handleSelect}
						defaultValue="Domaine"
						>
						<option value="domaine">Domaine</option>
						<option value="annee">Annee</option>
						<option value="batiment">Batiment</option>
						<option value="marque">Marque</option>
						<option value="nom">Nom</option>
					</select>
					<input
						className='form-control d-inline m-3 w-50'
						placeholder='entrer un domaine'
						value={this.state.domaine}
						type="text"
						name="domaine"
						autoComplete="given-domaine"
						onChange={this.handleInput} />
					<button
						className='btn btn-outline-primary m-3'
						type="submit"><i className="fas fa-search"></i></button>
				</div>
			</form>
		)
	}
}

export default DomainInput
