import React, { Component } from 'react';
import EquipementNode from './EquipementNode.jsx';

class EquipementList extends Component {
	constructor({ lstEquipement }) {
		super();
		this.state = {
			lstEquipement: lstEquipement
		}
	}

	render() {
		const { lstEquipement } = this.state
		return (
			<div
				className="List"
			>
				{
					lstEquipement.map((data, index) => {
						if (data.nom.length === 0)
							return (<div key={index}></div>);
						return (
							<EquipementNode
								data={data}
								key={index}
							/>
						)
					})
				}
			</div>
		)
	}
}

export default EquipementList
