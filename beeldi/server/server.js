var express = require('express');
var fs = require('fs');
var { error, sendListEquipement, queryEquipement } = require('./src/utils.js')
var equipements = require('./src/equipements.json');
var checkpoints = require('./src/checkpoints.json');

const port = 8080;
const server = express();

server.use((req, res, next) => {
		// allow request from localhost on port 3000
		res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
		next();
	})
	.get('/api/photos/:name', async (req, res) => {
		// send picture
		console.log('request photo', req.params.name);
		fs.readFile(`../public/Photos/${req.params.name}`, (err, data) => {
			if (err)
				return error(err, res, 'image not found');
			res.setHeader('Content-type', 'image/jpeg');
			res.send(data);
		})
	})
	.get('/api/equipement/list', async (req, res) => {
		// send list equipment
		sendListEquipement(res, equipements);
	})
	.get('/api/defaut/list', async (req, res) => {
		// send list defaut
		console.log('request defaut list')
		res.setHeader('Content-type', 'application/json');
		res.json(checkpoints);
	})
	.get('/api/equipement/query/:type/:name', async (req, res) => {
		// query request
		queryEquipement(req, res, equipements, checkpoints);
	})
	.get('/api/defaut/query/id/:id', async (req, res) => {
		// send defaut link to equipement of id 'id'
		console.log('request query defaut by id : ', req.params.id);
		let id = Number(req.params.id);
		let query = [];
		if (!isNaN(id))
			query = checkpoints.filter(({
				_EquipementID
			}) => (Number(_EquipementID) === id));
		res.setHeader('Content-type', 'application/json');
		res.json(query);
	})
	.use((req, res, next) => {
		// when connect to other page
		console.log('not found', req.originalUrl);
		fs.readFile('./src/notFound.html', 'utf-8', (err, data) => {
			if (err)
				return error(err, res, 'error when read notFound.html');
			res.setHeader('Content-type', 'text/html');
			res.status(404).send(data);
		})
	});
server.listen(port, () => console.log('listent on port ' + port));
