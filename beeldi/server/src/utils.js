const hash_key = {
	domaine: 'info_domaine',
	annee: 'info_anneeMiseEnService',
	batiment: 'info_batiment',
	marque: 'info_marque',
	nom: 'nom',
	nb: 'count',
	id: 'id'
}

function error(err, res, msg) {
	// when a error ocur send msg to client an log the error
	console.error(err);
	res.setHeader('Content-type', 'text/plain')
	res.status(500).send(msg)
}

function sendListEquipement(res, equipements) {
	// send the list of equipement
	console.log('request equipement list')
	res.setHeader('Content-type', 'application/json');
	res.json(equipements);
}

function queryEquipementCount(key, res, checkpoints) {
	// count number of defaut form equipement
	let id = Number(key);
	let nb = 0;
	if (!isNaN(id)) {
		for (let i = 0; i < checkpoints.length; i++) {
			if (Number(checkpoints[i]._EquipementID) === id)
				nb++;
		}
	}
	res.setHeader('Content-type', 'text/plain');
	return res.send(String(nb));
}

function queryEquipementId(equipements, key) {
	let id = Number(key);
	if (isNaN(id))
		return [];
	return equipements.filter(({
			__equipementID
		}) => (Number(__equipementID) === id));
}

function queryEquipementType(equipements, type, key) {
	return equipements.filter((data) => {
		if (typeof data[type] === 'undefined')
			return false
		return (data[type].toUpperCase().indexOf(key) !== -1);
	});
}

function queryEquipement(req, res, equipements, checkpoints) {
	console.log(`request query equipement ${req.params.type} ${req.params.name}`);
	let key = req.params.name.toUpperCase();
	const type = hash_key[req.params.type.toLowerCase()]
	if (type === 'count') {
		return queryEquipementCount(key, res, checkpoints)
	}
	if (type === 'id') {
		query = queryEquipementId(equipements, key)
	} else {
		query = queryEquipementType(equipements, type, key);
	}
	res.setHeader('Content-type', 'application/json');
	res.json(query);
}

module.exports = {
	error: error,
	sendListEquipement: sendListEquipement,
	queryEquipement: queryEquipement
}
