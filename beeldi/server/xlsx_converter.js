var xlsx = require('xlsx-to-json-lc');

function convert_xlsx_json(input, output) {

	if (input === undefined || output === undefined)
		return console.error("missing argument");

	console.log("convert file ", input)
	xlsx({
		input: input,
		output: output,
		loweCaseHeader: true
	}, (err, data) => {
		if (err)
			return console.log(err);
		console.log("done : ", input);
	})
}

convert_xlsx_json(
	"../public/20180105_checkpoints.xlsx",
	"./src/checkpoints.json"
);
convert_xlsx_json(
	"../public/20180105_equipements.xlsx",
	"./src/equipements.json"
);
