# beeldi-test
beeldi-test

# Usage
## Server
> How to launch the server
```
cd server
npm install
npm start
```
It will :
1. Install `dependencies`.
1. Create the json file.
1. Launch the server.

If you see :
```
listent on port 8080
```
Then you do it right

***
## Client

> How to launch the client

```
cd client
npm install
npm start
```
It will :
1. Install `dependencies`.
1. Start the client.

If you see :
```
Compiled successfully!

You can now view client in the browser.

  Local:            http://localhost:3000/
  On Your Network:  http://xxx.xxx.xxx.xxx:3000/

Note that the development build is not optimized.
To create a production build, use npm run build.
```
Then you do it right

# Info
## Server
> how the server work
for the server, I use:
- `express.js` a package that help buildin the server
- `xlsx-to-json-lc` a package that convert xlsx file in json one

The server basically wait for request and send `ajax` reply or `jpeg` picture.
In first, he requires the json file create by the `xlsx_converter.js` program

```
to see all posible request goto to http://localhost:8080
on your navigator
```

***
## Client
> how the client work
for the client, I use:
- `axios` for the `ajax` request
- `react` the base of the app
- `react-router-dom` to change the url without reloading the page
- `react-scripts` to lauch the client
- `create-react-app` that buid a skeleton for the app in `react`
- `react-spinner` to add spinner when request `data`

On the main page, the client send `ajax` request to the server to get the list of `equipement`.
For each `equipement`, it requests the number of `defaut` and his picture if exist.

On the page of an `equipement`, the client request the photo and the list of the `defaut`.

The client use `react-router` to navigated trough url without reloading the page.
To go to an equipement page you just have to click on the equipement that intereset you
